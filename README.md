Plasma Icinga Monitor
----------------------

Plasmoid to track services monitored by icinga2 instance from your plasma desktop. It is inspired by first icinga monitor done in https://github.com/MarkusH/plasma-icinga but is adapted to plasma 5, and icinga2 .

It requires to:

- Have an Icinga2 server with its api configured-
- Build and install the plasma icinga2_dataengine found in https://gitlab.com/aleixq/icinga2-dataengine/ , .

Once what's above is there and icinga2 monitor plasmoid is placed in a panel, it will create a piechart in the panel to quick view the states of the monitored services(as done in tactical view in icingaweb2), the tooltip of that piechart will show the service states counters.

When pie chart is clicked a new panel will appear with three tabs:

- Services: A list with the services and its basic information: service name, host, zone. Items are expandable to show the last check output and two buttons:
  - More information(it will open a slidepage with raw json information about the service) and
  - Link to Icingaweb, to show the service in icingaweb (if configured the link of icingaweb in plasmoid settings)
  Items can be searched by name, and grouped by states, by host name, or by zone.
- Hosts: super basic as in my case it's used a lesser than services. It will just list the hosts, without any information(PR are welcome)
- Tactical: A Pie chart with the states of the services and a legend with the counters of the state. 


## Build instructions

```
cd /where/your/applet/is/generated
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=MYPREFIX .. 
make 
make install
```

(MYPREFIX is where you install your Plasma setup, replace it accordingly)

Restart plasma to load the applet 
(in a terminal type: 
`kquitapp plasmashell`
and then
`plasmashell`)

or view it with 
`plasmoidviewer -a icinga_monitor`

## Tutorials and resources about plasmoiding

https://techbase.kde.org/Development/Tutorials/Plasma5/QML2/GettingStarted

Plasma QML API explained
https://techbase.kde.org/Development/Tutorials/Plasma2/QML2/API


## TODO: 

- chartview from services model.
- hosts from models
- notifications?
- Acknowledgements
- Issues
- Include certain  states only filters.


