// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL 
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
import QtQuick 2.11
import org.kde.plasma.configuration 2.0

ConfigModel {
    ConfigCategory {
        name: i18n("General")
        icon: "configure"
        source: "configGeneral.qml"
    }
    ConfigCategory {
        name: i18n("Icinga Web")
        icon: "services"
        source: "configIcingaWeb.qml"
    }
}
