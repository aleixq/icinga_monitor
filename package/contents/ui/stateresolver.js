.pragma library

var IcingaStates = {
    "Services": {
                                0 : {"text" : "Ok" , "color" : "#4b7"},
                                1 : {"text" :"Warning", "color" : "#fa4"},
                                4 : {"text" : "WarningHandled", "color" : "#fc6"},
                                2 : {"text" : "Critical", "color" : "#f56"},
                                5 : {"text" : "CriticalHandled", "color" : "#f9a"},
                                3 : {"text" : "Unknown", "color" : "#a4f"},
                                6 : {"text" : "UnknownHandled", "color" : "#c7f"},
                            },
        "Hosts": {
                                0 : {"text" : "Up", "color" : "#4b7"},
                                1 : {"text" : "Down", "color" : "#f56"},
                                3 : {"text" : "DownHandled", "color" : "#f9a"},
                                2 : {"text" : "Unreachable", "color" : "#a4f"},
                                4 : {"text" : "UnreachableHandled", "color" : "#c7f"},
                            },
}
