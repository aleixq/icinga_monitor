// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
import QtQuick.Controls 2.15
import org.kde.quickcharts 1.0 as Charts
import QtQuick 2.11
import "stateresolver.js" as StateResolver

Charts.PieChart {
    id: chart
    thickness: 20
    /*property alias okSlice  : okSlice
    property alias warningSlice  : warningSlice
    property alias warningHandledSlice : warningHandledSlice
    property alias criticalSlice : criticalSlice
    property alias criticalHandledSlice : criticalHandledSlice
    property alias unknownSlice : unknownSlice
    property alias unknownHandledSlice : unknownHandledSlice*/

    anchors.fill: parent
    //smoothEnds:true

    /*valueSources: Charts.ModelSource {
        roleName: "data";
        model: ListModel {
            id: listModel
            ListElement { data: counterServicesOk }
            ListElement { data: counterServicesWarning }
            ListElement { data: counterServicesWarningHandled }
            ListElement { data: counterServicesCritical }
            ListElement { data: counterServicesCriticalHandled }
            ListElement { data: counterServicesUnknown }
            ListElement { data: counterServicesUnknownHandled }

        }
    }*/
    valueSources:Charts.ArraySource {
    array: [
                counterServicesOk,
                counterServicesWarning,
                counterServicesWarningHandled,
                counterServicesCritical,
                counterServicesCriticalHandled,
                counterServicesUnknown,
                counterServicesUnknownHandled
            ]
}

    colorSource: Charts.ArraySource { array: [
            StateResolver.IcingaStates["Services"][0].color,
            StateResolver.IcingaStates["Services"][1].color,
            StateResolver.IcingaStates["Services"][4].color,
            StateResolver.IcingaStates["Services"][2].color,
            StateResolver.IcingaStates["Services"][5].color,
            StateResolver.IcingaStates["Services"][3].color,
            StateResolver.IcingaStates["Services"][6].color
        ] }
    nameSource: Charts.ArraySource { array: [
            "Ok",
            "Warning",
            "Warning handled",
            "Critical",
            "Critical handled",
            "Unknown",
            "Unknown handled"
        ] }

}
