// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL 
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
import QtQuick 2.7
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import org.kde.kirigami 2.10 as Kirigami

Item {
    id: page
    width: childrenRect.width
    height: childrenRect.height

    property alias cfg_icingaWebUrl: icingaWebUrl.text

    Kirigami.FormLayout {
        anchors.left: parent.left
        anchors.right: parent.right

        TextField {
            id: icingaWebUrl
            Kirigami.FormData.label: i18n("Icinga Web Url:")
            placeholderText: i18n("https://youricingaweb.org")
        }
        Label {
            text: i18n("This is the icinga web as %1, not the icinga server.", "https://icinga.com/docs/icingaweb2/latest/")
        }
    }
}
