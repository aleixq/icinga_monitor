﻿// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
import QtQuick 2.11
import QtQuick.Layouts 1.3
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.1 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

import org.kde.kirigami 2.10 as Kirigami
import org.kde.kitemmodels 1.0

//import QtQuick.Controls 2.15 as Controls
import QtQuick.Controls 2.15


Item {
    id:root
    property var servicesSearchSortFilterModel : []
    property int counterServicesOk : 0
    property int counterServicesWarning : 0
    property int counterServicesWarningHandled : 0
    property int counterServicesCritical : 0
    property int counterServicesCriticalHandled : 0
    property int counterServicesUnknown : 0
    property int counterServicesUnknownHandled : 0
    property bool fetchError : false
    property string errorString: ""

    property string tooltip: "Waiting for data"

    function dbg(ob){
        console.debug("[DBG]", JSON.stringify(ob, null, 2));
        console.debug("[DBG] Trace:")
        console.trace()
    }
    Timer {
        id: servicesFetchTimer
        running: false
        repeat: true
        onTriggered: {

            function activeUpdateModelsData(job) {
                //dbg(job)
                if (!job.result){
                    // job not succesful, but is handled in error datamodel below
                }

                // This will only work for updates doen via serviceForSource, not when confiugred globally with icingarc configfile
                // Some models are not ready to obtain data from some models, TODO make it ready as services do...
                //dbg("ServiceJob result: " + job.result + " op: " + job.operationName);
                //hosts = icingaDataSource.data["https://monitor.communia.org:5665/v1/objects/services"].hosts
                //hosts_up = icingaDataSource.data["https://monitor.communia.org:5665/v1/objects/services"].hostsCount
                // Set hosts:
                //hosts = icingaDataSource.data["https://monitor.communia.org:5665/v1/objects/services"].hosts
            }

            // Normalize the interval to a reasonable time
            interval = plasmoid.configuration.icingaApiRequestsInterval
            var service = icingaDataSource.serviceForSource("services")
            var operation = service.operationDescription("fetchServices")
            operation["icingaUrl"] = plasmoid.configuration.icingaUrl
            operation["icingaApiUser"] = plasmoid.configuration.icingaApiUser
            operation["icingaApiKey"] = plasmoid.configuration.icingaApiKey
            var serviceJob = service.startOperationCall(operation);
            serviceJob.finished.connect(activeUpdateModelsData)

        }
    }

    PlasmaCore.DataSource {
        id: icingaDataSource
        property var icingaObjects : [ "services" ] //TODO
        //^ expand to "hosts", "hostgroups", "servicegroups" ... https://icinga.com/docs/icinga2/latest/doc/12-icinga2-api/#icinga2-api-config-objects-query
        // whenever it is implemented in dataengine.
        engine: "icinga2"
        connectedSources: [ "services" ]
        //interval: plasmoid.configuration.icingaApiRequestsInterval
        //intervalAlignment: PlasmaCore.Types.AlignToMinute
        onSourceConnected: {
            // First time trigger
            if ( icingaObjects.includes(source) ) {
                //console.debug("Connecting source to request service")
                // this is the query about what icingaObjects are available
                // that is icinga2 api objects query implementations. Will start
                // timer if
                //if (plasmoid.configuration.icingaUrl !== "" && plasmoid.configuration.icingaApiUser !== "" &&  plasmoid.configuration.icingaApiKey !== "" ){
                // If operating via serviceForSource instead of setting icingarc
                servicesFetchTimer.running = true
                //}
                // TODO pause timer if cannot connect, and let start manually...
            } else {
                //console.debug("Connected to configured Url to Request Data for " + source)
                // Once a new Endpoint is connected let's link that model to Model that feeds the view
                servicesSearchSortFilterModel = this.models[source]
                servicesCountersModel.filterSource = source
                errorDataModel.sourceFilter = source
            }
        }
        onSourceDisconnected: {
        }

        onSourceAdded: { connectSource(source)}
        onSourceRemoved: disconnectSource(source)
        function restart(){
            connectedSources = []
            connectedSources = ["services", plasmoid.configuration.icingaUrl ]
        }
    }


    // TODO filter as it should to pick data from model instead of ugly and not smart refreshed arrays
    PlasmaCore.DataModel {
        id: hostsDataModel
        dataSource: icingaDataSource
        keyRoleFilter: "hosts"
    }

    PlasmaCore.SortFilterModel {
        // 2depth tuple access
        id: hostsModel
        filterRole: "hosts"
        filterRegExp: ".*"
        sourceModel: hostsDataModel
    }



    PlasmaCore.SortFilterModel {
        id: errorModel
        filterRole: "errorMessage"
        sourceModel:PlasmaCore.DataModel {
            id: errorDataModel
            dataSource: icingaDataSource
            //sourceFilter: // No specified means everything (dynamic set above^)
        }
        onDataChanged: {
            errorString = get(0).errorMessage
            fetchError = get(0).errorMessage.length > 0
        }
    }

    // TODO filter as it should to pick data from model instead of ugly and not smart refreshed arrays
    PlasmaCore.SortFilterModel {
        // 2depth tuple access
        id: servicesCountersModel
        property string filterSource
        filterRegExp: ".*"
        filterRole: "counters"
        sourceModel: PlasmaCore.DataModel {
            id: servicesCountersDataModel
            dataSource: icingaDataSource
            keyRoleFilter: "counters"
        }
        onDataChanged: {
            //console.debug("countersModel changed")
            if (filterSource){
                tooltip = prettyCounters(icingaDataSource.data[filterSource]['counters'])
                counterServicesOk = icingaDataSource.data[filterSource]['counters']["Ok"]
                counterServicesWarning = icingaDataSource.data[filterSource]['counters']["Warning"]
                counterServicesWarningHandled = icingaDataSource.data[filterSource]['counters']["Warning handled"]
                counterServicesCritical = icingaDataSource.data[filterSource]['counters']["Critical"]
                counterServicesCriticalHandled = icingaDataSource.data[filterSource]['counters']["Critical handled"]
                counterServicesUnknown = icingaDataSource.data[filterSource]['counters']["Unknown"]
                counterServicesUnknownHandled = icingaDataSource.data[filterSource]['counters']["Unknown handled"]

            }
        }

    }

    function prettyCounters(counters){
        var prettier = ""
        for (const prop in counters){
            prettier += prop + ": " + counters[prop] + "\n"
        }
        return prettier
    }


    Plasmoid.fullRepresentation:FullRepresentation{
        id : full
    }

    Plasmoid.backgroundHints: PlasmaCore.Types.DefaultBackground | PlasmaCore.Types.ConfigurableBackground
    Plasmoid.switchWidth: Plasmoid.formFactor === PlasmaCore.Types.Planar
                          ? -1
                          : (Plasmoid.fullRepresentationItem ? Plasmoid.fullRepresentationItem.Layout.minimumWidth : units.gridUnit * 8)
    Plasmoid.switchHeight: Plasmoid.formFactor === PlasmaCore.Types.Planar
                           ? -1
                           : (Plasmoid.fullRepresentationItem ? Plasmoid.fullRepresentationItem.Layout.minimumHeight : units.gridUnit * 12)
    Plasmoid.preferredRepresentation: Plasmoid.formFactor === PlasmaCore.Types.Planar ? Plasmoid.fullRepresentation : null
    Plasmoid.title: i18n("Icinga monitor")
    Plasmoid.toolTipSubText: tooltip
    Plasmoid.configurationRequired: plasmoid.configuration.icingaUrl.length === 0 || plasmoid.configuration.icingaApiUser.length === 0 || plasmoid.configuration.icingaApiKey.length === 0 || fetchError
    Plasmoid.configurationRequiredReason: fetchError? errorString : i18n("Plasmoid needs Icinga API credentials")
    Plasmoid.compactRepresentation: StatsCompactRepresentation {
        Repeater{
            model: errorModel
            delegate:
                ColumnLayout {
                anchors.fill: parent
                Layout.alignment: Qt.AlignHCenter
                visible: fetchError
                PlasmaCore.IconItem {
                    id: errorIcon
                    Layout.alignment: Qt.AlignHCenter
                    Layout.preferredWidth: units.iconSizes.huge
                    Layout.preferredHeight: width
                    source: {
                        if (icingaDataSource.data[model["DataEngineSource"]]['errorMessage'].length > 1){
                            tooltip = icingaDataSource.data[model["DataEngineSource"]]['errorMessage']
                            return "network-disconnect"
                        } else {
                            return "network-connect"
                        }
                    }
                }
                /* Verbose visual errors applet...
                Label {
                    id: error
                    Layout.fillWidth: true
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    visible: icingaDataSource.data[model["DataEngineSource"]]['errorMessage'].length > 1
                    text:{
                    console.debug(JSON.stringify({
                    "globfetchError" : fetchError,
                    "globerrorMessage": errorString,
                    "concreteError": icingaDataSource.data[model["DataEngineSource"]]['errorMessage'],
                    "concreteErrorSource:" : model["DataEngineSource"] }))
                    return icingaDataSource.data[model["DataEngineSource"]]['errorMessage']
                    }
                }
                */
            }
        }
    }

}
