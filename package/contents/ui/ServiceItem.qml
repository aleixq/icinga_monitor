// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
import QtQuick 2.6
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.15
import QtQml.Models 2.14

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras

import org.kde.kquickcontrolsaddons 2.0
import org.kde.kirigami 2.10 as Kirigami
import "stateresolver.js" as StateResolver

Kirigami.AbstractListItem {
    id: serviceItem
    // It's awaiting for a data props as:
    //    "displayName: " + displayName + "\n" +
    //    "serviceId: " + serviceId + "\n" +
    //    "hostName: " + hostName + "\n" +
    //    "lastCheckOutput: " + lastCheckOutput + "\n" +
    //    "zone: " + zone + "\n" +
    //    "service state: " + serviceState + "\n" +
    //    "raw: " + JSON.stringify(raw, null, 2) + "\n"

    property string status : serviceState

    backgroundColor: Kirigami.Theme.backgroundColor
    highlighted: ListView.isCurrentItem

    onEnabledChanged: if (!enabled) {
                          layout.extended = false;
                      }

    ColumnLayout {
        id: layout
        property bool extended: false
        onExtendedChanged: if (extended) {
                               //updateModel.fetchUpdateDetails(index)

                           }
        RowLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle {
                width: Kirigami.Units.gridUnit * 2
                Layout.preferredHeight: width
                id: status_indicator
                Layout.alignment: Qt.AlignVCenter|Qt.AlignLeft
                border.width:1
                border.color: StateResolver.IcingaStates["Services"][status].color
                radius: Kirigami.Units.smallSpacing / 2
                opacity: 0.7
                color: StateResolver.IcingaStates.Services[status].color
                Label {
                    elide: Text.ElideRight
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: StateResolver.IcingaStates["Services"][status].text
                    //opacity: listItem.hovered? 0.8 : 0.6
                    font.pointSize: theme.smallestFont.pointSize*0.5
                }

            }
            ColumnLayout {
                Layout.fillWidth: true
                // App name
                Kirigami.Heading {
                    Layout.fillWidth: true
                    text: "<b>" + displayName + "</b> on <b>" + hostName + "</b>"
                    level: 4
                    elide: Text.ElideRight
                }

                // Version numbers
                Label {
                    Layout.fillWidth: true
                    elide: truncated ? Text.ElideLeft : Text.ElideRight
                    text: (zone)? hostName + " from zone:" + zone: hostName
                    font:theme.smallestFont
                    //opacity: listItem.hovered? 0.8 : 0.6
                }
            }
        }

        Frame {
            Layout.fillWidth: true
            implicitHeight: view.contentHeight + Kirigami.Units.largeSpacing
            visible: layout.extended && lastCheckOutput.length>0
            ColumnLayout{
                anchors.fill:parent
                Label {
                    id: view
                    Layout.fillWidth: true
                    text: lastCheckOutput
                    textFormat: Text.AutoText
                    wrapMode: Text.WordWrap
                    onLinkActivated: Qt.openUrlExternally(link)
                    font.pointSize:theme.smallestFont.pointSize *0.7
                    font.family: "monospace"

                }
            }

            //This saves a binding loop on implictHeight, as the Label
            //height is updated twice (first time with the wrong value)
            Behavior on implicitHeight
            { PropertyAnimation { duration: Kirigami.Units.shortDuration } }
        }
        RowLayout{
            Layout.alignment: Qt.AlignRight
            Button {
                Layout.alignment: Qt.AlignRight
                text: i18n("More Information...")
                visible: layout.extended
                onClicked: {
                    overlayDialogBody = JSON.stringify(
                                raw,
                                null, 2)
                    pageStack.push(overlayDialog, {
                                       "id": overlay
                                   })
                }
            }
            Button {
                Layout.alignment: Qt.AlignRight
                text: i18n("Icingaweb")
                visible: layout.extended && plasmoid.configuration.icingaWebUrl.length !== ""
                onClicked: {
                    Qt.openUrlExternally( "%1/monitoring/service/show?host=%2&service=%3".
                                         arg(plasmoid.configuration.icingaWebUrl)
                                         .arg(hostName)
                                         .arg(displayName))
                }
            }
        }
    }

    onClicked: {
        layout.extended = !layout.extended
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:3}
}
##^##*/
