// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
import QtCharts 2.3

import "stateresolver.js" as StateResolver

ChartView {
    id: chart
    
    property alias okSlice  : okSlice
    property alias warningSlice  : warningSlice
    property alias warningHandledSlice : warningHandledSlice
    property alias criticalSlice : criticalSlice
    property alias criticalHandledSlice : criticalHandledSlice
    property alias unknownSlice : unknownSlice
    property alias unknownHandledSlice : unknownHandledSlice
    
    title: "Service Summary"
    anchors.fill: parent
    antialiasing: true
    animationOptions: ChartView.AllAnimations
    backgroundColor: 'transparent'
    legend.alignment: Qt.AlignRight
    legend.font.pointSize: 6
    legend.showToolTips : true
    theme:ChartView.ChartThemeDark
    //legend.color: theme.textColor //PlasmaCore.Theme.them
    //TODO DELEGATE LEGEND to be CLICKABLE TO SERVICE VIEW (with applied filter)
    anchors { fill: parent; margins: 0 }
    margins { right: 0; bottom: 0; left: 0; top: 0 }

    PieSeries {
        id: servicesSeries
        size: 2
        holeSize: 0.6
        /*Component.onCompleted : {
        *                    //okSlice = pieSeries.find("Ok") // Not valid as value is prepended...
        *                    //othersSlice = pieSeries.append("Others", 52.0);
        * }
        */
        PieSlice { id: okSlice; label: value + " Ok"; value: counterServicesOk; color: StateResolver.IcingaStates["Services"][0].color; borderWidth:0 }
        PieSlice { id: warningSlice; label: value + " Warning"; value: counterServicesWarning; color: StateResolver.IcingaStates["Services"][1].color; borderWidth:0 }
        PieSlice { id: warningHandledSlice;  value: counterServicesWarningHandled; label: value + " Warning handled"; color: StateResolver.IcingaStates["Services"][4].color; borderWidth:0 }
        PieSlice { id: criticalSlice;  value: counterServicesCritical; label: value + " Critical"; color: StateResolver.IcingaStates["Services"][2].color; borderWidth:0 }
        PieSlice { id: criticalHandledSlice;  value: counterServicesCriticalHandled; label: value + " Critical handled"; color: StateResolver.IcingaStates["Services"][5].color; borderWidth:0 }
        PieSlice { id: unknownSlice;  value: counterServicesUnknown; label: value + " Unknown"; color: StateResolver.IcingaStates["Services"][3].color; borderWidth:0 }
        PieSlice { id: unknownHandledSlice;  value: counterServicesUnknownHandled; label: value + " Unknown handled"; color:  StateResolver.IcingaStates["Services"][6].color; borderWidth:0 }
        
        /*
        *  Example about adding series programmatically
        * Component.onCompleted : {
        *                    var myAxisX = chart.axisX(baseSerie);
        *                    var myAxisY = chart.axisX(baseSerie);
        *
        *                    var outerSerie = chart.createSeries(ChartView.SeriesTypePie, "Outer Serie", myAxisX, myAxisY);
        *                    outerSerie.append("okSlice", Math.random())
        *                    outerSerie.append("varSlice", Math.random())
        */
    }
}
